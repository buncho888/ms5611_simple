/*
  MS5611 Barometric Pressure & Temperature Sensor. Simple Example
  Read more: http://www.jarzebski.pl/arduino/czujniki-i-sensory/czujnik-cisnienia-i-temperatury-ms5611.html
  GIT: https://github.com/jarzebski/Arduino-MS5611
  Web: http://www.jarzebski.pl
  (c) 2014 by Korneliusz Jarzebski
*/

#include <Wire.h>
#include <MS5611.h>
#include "U8glib.h"

MS5611 ms5611;

// Set up the display
U8GLIB_SH1106_128X64 u8g(U8G_I2C_OPT_NONE);	// I2C / TWI 

double realTemperature, realPressure;
float absoluteAltitude, relativeAltitude;
double referencePressure;

#define MOVAVG_SIZE 8
float movavg_buff[MOVAVG_SIZE];
int movavg_i=0;
float avgAltitude;


void setup() 
{
  Serial.begin(9600);

  // Initialize MS5611 sensor
  Serial.println("Initialize MS5611 Sensor");

  while(!ms5611.begin())
  {
    Serial.println("Could not find a valid MS5611 sensor, check wiring!");
    delay(500);
  }

  // Get reference pressure for relative altitude
  referencePressure = ms5611.readPressure();   

  // Check settings
  checkSettings();
  
  // Read true temperature & Pressure
  realTemperature = ms5611.readTemperature();
  realPressure = ms5611.readPressure();

  // Calculate altitude
  absoluteAltitude = ms5611.getAltitude(realPressure);
  relativeAltitude = ms5611.getAltitude(realPressure, referencePressure);
  
  // populate movavg_buff before starting loop
  for(int i=0; i<MOVAVG_SIZE; i++) {
    
    movavg_buff[i] = relativeAltitude;
    
  }
}

void checkSettings()
{
  Serial.print("Oversampling: ");
  Serial.println(ms5611.getOversampling());
}

void loop()
{
  // Read raw values
  //uint32_t rawTemp = ms5611.readRawTemperature();
  //uint32_t rawPressure = ms5611.readRawPressure();

  // Read true temperature & Pressure
  realTemperature = ms5611.readTemperature();
  realPressure = ms5611.readPressure();

  // Calculate altitude
  absoluteAltitude = ms5611.getAltitude(realPressure);
  relativeAltitude = ms5611.getAltitude(realPressure, referencePressure);
  
 
  if(relativeAltitude != NULL) {
    pushAvg(relativeAltitude);
  }
  avgAltitude = getAvg(movavg_buff, MOVAVG_SIZE);


  //Serial.println("--");

  //Serial.print(" rawTemp = ");
  //Serial.print(rawTemp);
  //Serial.print(" realTemp = ");
  //Serial.print(realTemperature);
  //Serial.println(" *C");
/*
  Serial.print(" rawPressure = ");
  Serial.print(rawPressure);
  Serial.print(", realPressure = ");
  Serial.print(realPressure);
  Serial.println(" Pa");
  
  */

  //Serial.print(" abs alt: ");
  //Serial.print(absoluteAltitude);
  
  /*
  Serial.print(" m, rel alt: ");
  Serial.print(relativeAltitude);    
  Serial.print(" m, avg alt: ");
  Serial.print(avgAltitude);    
  Serial.println(" m");
  */
  
  // picture loop  
  u8g.firstPage();  
  do {
    u8g.setFont(u8g_font_lucasfont_alternate);
    u8g.setPrintPos(0, 20); 
    u8g.print(avgAltitude);
    u8g.print(" m");
  } while( u8g.nextPage() );
 
  delay(10);
}

void pushAvg(float val) {
  movavg_buff[movavg_i] = val;
  movavg_i = (movavg_i + 1) % MOVAVG_SIZE;
}

float getAvg(float * buff, int size) {
  float sum = 0.0;
  for(int i=0; i<size; i++) {
    sum += buff[i];
  }
  return sum / size;
}


